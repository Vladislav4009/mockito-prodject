package by.shag.gritskevich.mapping;

import by.shag.gritskevich.api.dto.UserDto;
import by.shag.gritskevich.jpa.model.User;
import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

class UserDtoMapperTest {

    private static final Integer ID = 1;
    private static final String NAME = "Vlad";
    private static final String LASTNAME = "Grit";
    private static final String LOGIN = "ProBishop";
    private static final String EMAIL = "ProBishop@gmail.com";
    private static final int AGE = 25;

    private UserDtoMapper userDtoMapper = new UserDtoMapper();

    @Test
    void map() {
        UserDto userDto = new UserDto();
        userDto.setId(ID);
        userDto.setName(NAME);
        userDto.setLastName(LASTNAME);
        userDto.setEmail(EMAIL);
        userDto.setAge(AGE);

        User user = userDtoMapper.map(userDto);

        assertEquals(user.getId(), ID);
        assertEquals(user.getName(), NAME);
        assertEquals(user.getLastName(), LASTNAME);
        assertEquals(user.getLogin(), LOGIN);
        assertEquals(user.getAge(), AGE);
    }

    @Test
    void testMap() {
        User user = new User();
        user.setId(ID);
        user.setName(NAME);
        user.setLastName(LASTNAME);
        user.setLogin(LOGIN);
        user.setAge(AGE);

        UserDto userDto = userDtoMapper.map(user);

        assertEquals(userDto.getId(), ID);
        assertEquals(userDto.getName(), NAME);
        assertEquals(userDto.getLastName(), LASTNAME);
        assertEquals(userDto.getLogin(), LOGIN);
        assertEquals(userDto.getAge(), AGE);
        assertNull(userDto.getEmail());
    }
}