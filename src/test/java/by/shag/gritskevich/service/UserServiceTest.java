package by.shag.gritskevich.service;

import by.shag.gritskevich.api.dto.UserDto;
import by.shag.gritskevich.jpa.model.User;
import by.shag.gritskevich.jpa.repository.UserRepository;
import by.shag.gritskevich.mapping.UserDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserDtoMapper userDtoMapper;
    @InjectMocks
    private UserService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(userDtoMapper, userRepository);
    }

    @Test
    void save() {
        User user = new User();
        user.setId(1);
        when(userDtoMapper.map(any(UserDto.class))).thenReturn(user);

        User saved = new User();
        saved.setId(2);
        when(userRepository.save(any(User.class))).thenReturn(saved);

        UserDto userDtoReturn = new UserDto();
        userDtoReturn.setId(3);
        when(userDtoMapper.map(any(User.class))).thenReturn(userDtoReturn);

        UserDto userDto = new UserDto();
        userDto.setId(4);
        UserDto result = service.save(userDto);
        assertEquals(userDtoReturn, result);

        verify(userDtoMapper).map(userDto);
        verify(userRepository).save(user);
        verify(userDtoMapper).map(saved);
    }

    @Test
    void findById() {
        Integer id = 1;

        User user = new User();
        user.setId(id);
        when(userRepository.findById(id)).thenReturn(user);

        UserDto userDto = new UserDto();
        userDto.setId(id);
        when(userDtoMapper.map(any(User.class))).thenReturn(userDto);

        UserDto result = service.findById(id);
        assertEquals(id, result.getId());

        verify(userRepository).findById(id);
        verify(userDtoMapper).map(user);

    }

    @Test
    void findAll() {
        User user = new User();
        user.setId(1);
        List<User> userList = Arrays.asList(user, user, user, user);
        when(userRepository.findAll()).thenReturn(userList);

        UserDto userDto = new UserDto();
        userDto.setId(1);
        List<UserDto> userDtoList = Arrays.asList(userDto, userDto, userDto, userDto);
        when(userDtoMapper.map(any(User.class))).thenReturn(userDto);

        List<UserDto> result = service.findAll();
        assertArrayEquals(userDtoList.toArray(), result.toArray());

        verify(userRepository).findAll();
        verify(userDtoMapper, times(4)).map(user);

    }

    @Test
    void update() {
        User user = new User();
        user.setId(1);
        when(userDtoMapper.map(any(UserDto.class))).thenReturn(user);

        User updated = new User();
        updated.setId(1);
        when(userRepository.update(any(User.class))).thenReturn(updated);

        UserDto userDtoReturn = new UserDto();
        userDtoReturn.setId(1);
        when(userDtoMapper.map(any(User.class))).thenReturn(userDtoReturn);

        UserDto userDtoSend = new UserDto();
        userDtoSend.setId(1);
        UserDto result = service.update(userDtoSend);
        assertEquals(userDtoReturn, result);

        verify(userDtoMapper).map(userDtoSend);
        verify(userRepository).update(user);
        verify(userDtoMapper).map(updated);
    }

    @Test
    void deleteById() {
        Integer id = 1;

        service.deleteById(id);

        verify(userRepository).deleteById(id);
    }
}