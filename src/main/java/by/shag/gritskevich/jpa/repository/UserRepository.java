package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.jpa.model.User;

import java.util.List;

public class UserRepository {

    public User save(User user) {
        throw new UnsupportedOperationException();
    }

    public User findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    public List<User> findAll() {
        throw new UnsupportedOperationException();
    }

    public User update(User user) {
        throw new UnsupportedOperationException();
    }

    public void deleteById(Integer id) {
        throw new UnsupportedOperationException();
    }
}
