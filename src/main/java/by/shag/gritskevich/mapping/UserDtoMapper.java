package by.shag.gritskevich.mapping;

import by.shag.gritskevich.api.dto.UserDto;
import by.shag.gritskevich.jpa.model.User;

public class UserDtoMapper {

    public User map(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setLastName(userDto.getLastName());
        if (userDto.getLogin() != null) {
            user.setLogin(userDto.getLogin());
        } else {
            user.setLogin(userDto.getEmail());
        }
        user.setAge(userDto.getAge());
        return user;
    }

    public UserDto map(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setLastName(user.getLastName());
        userDto.setLogin(user.getLogin());
        userDto.setAge(user.getAge());
        return userDto;
    }
}
