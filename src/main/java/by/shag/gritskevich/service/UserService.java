package by.shag.gritskevich.service;

import by.shag.gritskevich.api.dto.UserDto;
import by.shag.gritskevich.jpa.model.User;
import by.shag.gritskevich.jpa.repository.UserRepository;
import by.shag.gritskevich.mapping.UserDtoMapper;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserRepository userRepository;
    private UserDtoMapper userDtoMapper;


    public UserDto save(UserDto userDto) {
        User user = userDtoMapper.map(userDto);
        User saved = userRepository.save(user);
        return userDtoMapper.map(saved);
    }

    public UserDto findById(Integer id) {
        User user = userRepository.findById(id);
        return userDtoMapper.map(user);
    }

    public List<UserDto> findAll() {
        List<UserDto> users = userRepository.findAll()
                .stream()
                .map(user -> userDtoMapper.map(user))
                .collect(Collectors.toList());
        return users;
    }

    public UserDto update(UserDto userDto) {
        User user = userDtoMapper.map(userDto);
        User updated = userRepository.update(user);
        return userDtoMapper.map(updated);
    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }
}
